I = imread('../assets/bai2.bmp');
IEs = edge(I, 'sobel');
IEc = edge(I, 'Canny', [0 0.2]);
subplot(2,2,1), imshow(I);
title('Original','FontWeight','bold')
subplot(2,2,2), imshow(IEs);
title('Sobel','FontWeight','bold')
subplot(2,2,3), imshow(IEc);
title('Canny','FontWeight','bold')