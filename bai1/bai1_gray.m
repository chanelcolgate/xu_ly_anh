subplot(221)
A = imread('../assets/bai1_gray.png');
imshow(A)
title('Original','FontWeight','bold')
B = rgb2gray(A);
FT = fft2(B);

[M, N] = size(FT);
mask = zeros(M, N);
[fy, fx] = ndgrid(0:M/2, 0:N/2);
sigmafs = [5, 20, 100]
for n = 2:4
sigmaf = sigmafs(n-1);
% Gaussian mask centred on zero frequency
mask(1:M/2+1, 1:N/2+1) = exp(-(fx.^2+fy.^2)/(2*sigmaf)^2);
% Do symmetries
mask(1:M/2+1, N:-1:N/2+2) = mask(1:M/2+1, 2:N/2);
mask(M:-1:M/2+2, :) = mask(2:M/2, :);
% Filter the FT and show the result
imfilt = ifft2(mask.*FT);
subplot(2,2,n); imshow(imfilt, []);
title(['sigmaf = ' int2str(sigmaf)], 'FontWeight', 'bold')
end