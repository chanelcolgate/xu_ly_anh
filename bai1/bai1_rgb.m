subplot(221)
A = imread('../assets/bai1_rgb.png');
imshow(A)
title('Original','FontWeight','bold')
B = rgb2gray(A);
BRed = A(:,:,1);
BGreen = A(:,:,2);
BBlue = A(:,:,3);

FTRed=fft2(BRed);
FTGreen=fft2(BGreen);
FTBlue=fft2(BBlue);

sigmafs = [5, 20, 100]
for n = 2:4
sigmaf = sigmafs(n-1);
imfiltRed = lowPassFilter(FTRed, sigmaf);
imfiltGreen = lowPassFilter(FTGreen, sigmaf);
imfiltBlue = lowPassFilter(FTBlue, sigmaf);
b = uint8(cat(3, imfiltRed, imfiltGreen, imfiltBlue));
subplot(2,2,n); imshow(b, []);
title(['sigmaf = ' int2str(sigmaf)], 'FontWeight', 'bold')
end