subplot(221)
X = imread('../assets/bai3.jpg');
imshow(X)
title('Original','FontWeight','bold')
for n = 2:4
    IDX = otsu(X,n);
    subplot(2,2,n);
    imagesc(IDX), axis image off
    title(['n = ' int2str(n)],'FontWeight','bold')
end